package housepoints

const (
	LogHousePointsEventID                 = "housepoints.event.id"
	LogHousePointsSlackEventType          = "housepoints.event.slack.type"
	LogSubmissionMethodKey                = "housepoints.submission.method"
	LogSubmissionTypeKey                  = "housepoints.submission.type"
	LogSubmissionActionKey                = "housepoints.submission.action"
	LogSubmissionPointCountKey            = "housepoints.submission.amount"
	LogSubmissionSharedUrlKey             = "housepoints.submission.shared_url"
	LogSubmissionOriginatorSlackUserIDKey = "housepoints.submission.originator.slack.user.id"
	LogSubmissionReceiverSlackUserIDKey   = "housepoints.submission.recipient.slack.user.id"
	LogStorageID                          = "housepoints.storage.id"

	SubmissionMethodBehavior    = "behavior"
	SubmissionMethodDirect      = "direct"
	SubmissionTypeSlackReaction = "slack_reaction"
	SubmissionTypeSharedURL     = "shared_url"
	SubmissionTypeDirectAward   = "direct_award"
	SubmissionActionAdd         = "add"
	SubmissionActionRemove      = "remove"

	// Slack
	HousePointsServiceName          = "house_points"
	HousePointsSlashCommandPoints   = "/points" // Must match what Slack sends us
	HousePointsInteractionIDGlobal  = "house_points_global_interaction"
	HousePointsInteractionIDMessage = "house_points_message_interaction"
)
