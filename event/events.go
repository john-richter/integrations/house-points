package hpevent

import (
	"encoding/json"

	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
)

const (
	EventTypeAwardPoints = "award_points"
	EventTypeLinkShared  = "link_shared"
	EventTypeReaction    = "reaction"

	EventOriginSlack       = "slack"
	EventOriginHousePoints = "house_points"

	EventActionAdd    = "add"
	EventActionRemove = "remove"

	SlackEventTypeEventsAPI    = "events_api"
	SlackEventTypeInteraction  = "interaction"
	SlackEventTypeSlashCommand = "slash_command"
)

type User struct {
	ID string `json:"id"`
}

type Conversation struct {
	ID string `json:"id"`
}

type HousePointsEvent struct {
	ID        string `json:"id"`
	Type      string `json:"type"`
	Origin    string `json:"origin"`
	Event     interface{}
	EventData json.RawMessage       `json:"event"`
	Slack     HousePointsSlackEvent `json:"slack"`
}

type HousePointsSlackEvent struct {
	Type           string          `json:"type"`
	EventData      json.RawMessage `json:"event"`
	EventsAPIEvent HousePointsSlackAPIEvent
	Interaction    slack.InteractionCallback
	SlashCommand   slack.SlashCommand
}

type HousePointsSlackAPIEvent struct {
	Event    slackevents.EventsAPIEvent
	Callback slackevents.EventsAPICallbackEvent
}

type AwardPointsEvent struct {
	Type         string       `json:"type"`
	Conversation Conversation `json:"conversation"`
	Action       string       `json:"action"`
	Amount       int          `json:"amount"`
	Benefactor   User         `json:"benefactor"`
	Beneficiary  User         `json:"beneficiary"`
	Explanation  string       `json:"explanation"`
}

type LinkSharedEvent struct {
	Type         string       `json:"type"`
	Conversation Conversation `json:"conversation"`
	User         User         `json:"user"`
	Urls         []string     `json:"urls"`
}

type ReactionEvent struct {
	Type         string       `json:"type"`
	Conversation Conversation `json:"conversation"`
	Action       string       `json:"action"`
	Benefactor   User         `json:"benefactor"`
	Beneficiary  User         `json:"beneficiary"`
	Reaction     string       `json:"reaction"`
}
