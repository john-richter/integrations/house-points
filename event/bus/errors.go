package bus

var (
	ErrorTooManyRetries = SQSBusError{"Max retry attempts reached. Dropping message"}
)

type SQSBusError struct {
	Message string `json:"message"`
}

func (e *SQSBusError) Error() string {
	return e.Message
}
