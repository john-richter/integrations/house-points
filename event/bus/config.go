package bus

type EventBusConfig struct {
	MaxRetryAttempts int `json:"event_bus_max_retry_attempts" mapstructure:"event_bus_max_retry_attempts"`
}

func NewDefaultEventBusConfig() *EventBusConfig {
	return &EventBusConfig{
		MaxRetryAttempts: 0,
	}
}
