package bus

const (
	SpanOperationProcessEvent = "house_points.ProcessEvent"

	LogHousePointsBusMessageAttempt = "housepoints.bus.message.attempt"
	LogHousePointsBusMessageMax     = "housepoints.bus.message.max"
)
