package bus

import (
	"context"
	"sync"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/rs/zerolog/log"
	hp "gitlab.com/johnrichter/house-points"
	hpevent "gitlab.com/johnrichter/house-points/event"
	"gitlab.com/johnrichter/logging-go"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

type SqsEventBusConfig struct {
	SqsQueueUrl       string `json:"sqs_queue_url" mapstructure:"sqs_queue_url"`
	SqsMessageGroupId string `json:"sqs_message_group_id" mapstructure:"sqs_message_group_id"`
}

func NewDefaultSqsEventBusConfig() *SqsEventBusConfig {
	return &SqsEventBusConfig{
		SqsQueueUrl:       "",
		SqsMessageGroupId: "",
	}
}

type SQSEventBus struct {
	ebConfig                  *EventBusConfig
	config                    *SqsEventBusConfig
	consumer                  EventBusConsumer
	sqs                       *sqs.Client
	slackAppVerificationToken string
}

func NewSQSEventBus(
	ebc *EventBusConfig,
	c *SqsEventBusConfig,
	sqs *sqs.Client,
	p EventBusConsumer,
	slackVerificationToken string,
) *SQSEventBus {
	return &SQSEventBus{
		ebConfig:                  ebc,
		config:                    c,
		sqs:                       sqs,
		consumer:                  p,
		slackAppVerificationToken: slackVerificationToken,
	}
}

func (p *SQSEventBus) AddEvent(ctx context.Context, e *hpevent.HousePointsEvent) error {
	hpeJson, err := hpevent.MarshalHousePointsEventToJson(e)
	if err != nil {
		return err
	}
	span, _ := tracer.SpanFromContext(ctx)
	ma := NewSQSMessageAttributes(1, span.Context())
	m := p.sqsMessageInput(ma)
	m.MessageDeduplicationId = aws.String(hpevent.GenID("%s:%d", e.ID, ma.RetryAttempt))
	m.MessageGroupId = &p.config.SqsMessageGroupId
	m.MessageBody = aws.String(hpeJson)

	o, err := p.sqs.SendMessage(ctx, m)
	if err != nil {
		log.Error().
			Err(err).
			Str(hp.LogHousePointsEventID, e.ID).
			Str(logging.AwsSqsQueueUrl, p.config.SqsQueueUrl).
			Str(logging.AwsSqsMessageDeduplicationId, *m.MessageDeduplicationId).
			Str(logging.AwsSqsMessageGroupId, p.config.SqsMessageGroupId).
			Msg("Unable to add House Points event to SQS queue")
		return err
	}
	log.Debug().
		Str(hp.LogHousePointsEventID, e.ID).
		Str(logging.AwsSqsQueueUrl, p.config.SqsQueueUrl).
		Str(logging.AwsSqsMessageGroupId, p.config.SqsMessageGroupId).
		Str(logging.AwsSqsMessageID, *o.MessageId).
		Str(logging.AwsSqsMessageSequenceNumber, *o.SequenceNumber).
		Msg("Added House Points event to SQS queue")
	return nil
}

func (p *SQSEventBus) addMessageToBus(
	ctx context.Context,
	m *events.SQSMessage,
	ma *SQSMessageAttributes,
) error {
	mi := p.sqsMessageInputFromMessage(m, ma)
	o, err := p.sqs.SendMessage(ctx, mi)
	if err != nil {
		log.Error().
			Err(err).
			Str(logging.AwsSqsQueueUrl, p.config.SqsQueueUrl).
			Str(logging.AwsSqsMessageDeduplicationId, *mi.MessageDeduplicationId).
			Str(logging.AwsSqsMessageGroupId, p.config.SqsMessageGroupId).
			Msg("Unable to add message to SQS queue")
		return err
	}
	log.Debug().
		Str(logging.AwsSqsQueueUrl, p.config.SqsQueueUrl).
		Str(logging.AwsSqsMessageGroupId, p.config.SqsMessageGroupId).
		Str(logging.AwsSqsMessageID, *o.MessageId).
		Str(logging.AwsSqsMessageSequenceNumber, *o.SequenceNumber).
		Msg("Added message to queue")
	return nil
}

func (p *SQSEventBus) removeMesageFromBus(ctx context.Context, m *events.SQSMessage) error {
	mi := sqs.DeleteMessageInput{
		QueueUrl:      &p.config.SqsQueueUrl,
		ReceiptHandle: &m.ReceiptHandle,
	}
	_, err := p.sqs.DeleteMessage(ctx, &mi)
	if err != nil {
		log.Error().
			Err(err).
			Str(logging.AwsSqsQueueUrl, p.config.SqsQueueUrl).
			Str(logging.AwsSqsMessageGroupId, p.config.SqsMessageGroupId).
			Str(logging.AwsSqsMessageDeduplicationId, m.MessageId).
			Str(logging.AwsSqsMessageReceiptHandle, m.ReceiptHandle).
			Msg("Unable to remove message from SQS queue")
		return err
	}
	log.Debug().
		Str(logging.AwsSqsQueueUrl, p.config.SqsQueueUrl).
		Str(logging.AwsSqsMessageGroupId, p.config.SqsMessageGroupId).
		Str(logging.AwsSqsMessageID, m.MessageId).
		Str(logging.AwsSqsMessageReceiptHandle, m.ReceiptHandle).
		Msg("Removed message from SQS queue")
	return nil
}

func (p *SQSEventBus) ProcessMessages(ctx context.Context, msgs []events.SQSMessage) {
	var wg sync.WaitGroup
	for _, m := range msgs {
		ma := NewSQSMessageAttributesFromMessage(&m)
		wg.Add(1)
		go p.processMessage(ctx, &wg, &m, ma)
	}
	wg.Wait()
}

func (p *SQSEventBus) processMessage(
	ctx context.Context,
	wg *sync.WaitGroup,
	m *events.SQSMessage,
	ma *SQSMessageAttributes,
) {
	defer wg.Done()
	distributedSpan := tracer.StartSpan(SpanOperationProcessEvent, tracer.ChildOf(ma.DdParentSpanContext))
	defer distributedSpan.Finish()
	distributedSpanCtx := tracer.ContextWithSpan(ctx, distributedSpan)

	var err error
	var hpe *hpevent.HousePointsEvent

	if ma.RetryAttempt > p.ebConfig.MaxRetryAttempts {
		log.Error().
			Int(LogHousePointsBusMessageAttempt, ma.RetryAttempt).
			Int(LogHousePointsBusMessageMax, p.ebConfig.MaxRetryAttempts).
			Msg("Max processing attempts reached for HousePointsEvent. Event will be dropped.")
		p.removeMesageFromBus(distributedSpanCtx, m)
	} else if hpe, err = hpevent.UnmarshalHousePointsEventFromJson(m.Body, p.slackAppVerificationToken); err != nil {
		log.Debug().Err(err).Msg("Could not unmarshal HousePointsEvent JSON payload")
		p.removeMesageFromBus(distributedSpanCtx, m) // Message is corrupt
	} else {
		shouldReprocess := false
		if err := p.consumer.ProcessEvent(distributedSpanCtx, hpe); err != nil {
			log.Error().Err(err).Msg("Unable to process House Points event. Attempting to retry.")
			p.reprocessOrDrop(distributedSpanCtx, m, ma)
		}
		if shouldReprocess {
			p.reprocessOrDrop(distributedSpanCtx, m, ma)
		}
	}
}

func (p *SQSEventBus) reprocessOrDrop(ctx context.Context, m *events.SQSMessage, ma *SQSMessageAttributes) {
	ma.RetryAttempt += 1
	if ma.RetryAttempt > p.ebConfig.MaxRetryAttempts {
		log.Error().
			Int(LogHousePointsBusMessageAttempt, ma.RetryAttempt).
			Int(LogHousePointsBusMessageMax, p.ebConfig.MaxRetryAttempts).
			Msg("Max processing attempts reached for HousePointsEvent. Event will be dropped.")
		p.removeMesageFromBus(ctx, m)
	} else {
		log.Info().Msgf(
			"Attempting retry attempt %d of %d for HousePointsEvent",
			ma.RetryAttempt, p.ebConfig.MaxRetryAttempts,
		)
		p.addMessageToBus(ctx, m, ma)
	}
}

func (p *SQSEventBus) sqsMessageInput(ma *SQSMessageAttributes) *sqs.SendMessageInput {
	m := sqs.SendMessageInput{
		QueueUrl:          &p.config.SqsQueueUrl,
		MessageAttributes: ma.ToMessageAttributes(nil),
		MessageBody:       aws.String(""),
	}
	return &m
}

func (p *SQSEventBus) sqsMessageInputFromMessage(
	m *events.SQSMessage,
	ma *SQSMessageAttributes,
) *sqs.SendMessageInput {
	mi := sqs.SendMessageInput{
		QueueUrl:               &p.config.SqsQueueUrl,
		MessageAttributes:      ma.ToMessageAttributes(m),
		MessageDeduplicationId: aws.String(hpevent.GenID("%s:%d", m.MessageId, ma.RetryAttempt)),
		MessageGroupId:         aws.String(p.config.SqsMessageGroupId),
		MessageBody:            aws.String(m.Body),
	}
	return &mi
}
